<?php  require('../../action/core/url.php'); ?>
<li class="nav-section">
  <span class="sidebar-mini-icon">
    <i class="fa fa-ellipsis-h"></i>
  </span>
  <h4 class="text-section">Menu</h4>
</li>

<li class="nav-item active" >
  <a href="dashboard.php">
    <i class="fas fa-home"></i>
    <p>Dashboard</p>
  </a>
</li>

<li class="nav-item active" >
  <a href="anggota.php">
    <i class="fas fa-user"></i>
    <p>Anggota</p>
  </a>
</li>

<li class="nav-item active" >
  <a href="undian.php">
    <i class="fas fa-gift"></i>
    <p>Undian</p>
  </a>
</li>

<li class="nav-item active" >
  <a href="pemenang.php">
    <i class="fas fa-users"></i>
    <p>Pemenang</p>
  </a>
</li>
