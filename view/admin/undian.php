
<?php
  include('../../action/core/url.php');
  include('../../action/core/connection.php');
  if(isset($_POST['keyword']) and $_POST['keyword']!=""){
  //  var_dump($a);die;
    $keyword = $_POST['keyword'];
    $query = $database->query("select * from undian where nama LIKE '%".$keyword."%'");
  } else {
    $query = $database->query("select * from undian");
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>ARION</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="/assets/img/icon.ico" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="/assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['/assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/css/atlantis.min.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="/assets/css/demo.css">
</head>
<body>
	<div class="wrapper">
		<div class="main-header">
			<!-- Logo Header -->
			<div class="logo-header" data-background-color="blue">

				<a href="index.html" class="logo">
					<img src="/assets/img/logo.svg" alt="navbar brand" class="navbar-brand">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
				<div class="nav-toggle">
					<button class="btn btn-toggle toggle-sidebar">
						<i class="icon-menu"></i>
					</button>
				</div>
			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue2">

				<div class="container-fluid">
					<div class="collapse" id="search-nav">
						<form class="navbar-left navbar-form nav-search mr-md-3" method="post" target="">
							<div class="input-group">
								<div class="input-group-prepend">
									<button type="submit" name="search" class="btn btn-search pr-1">
										<i class="fa fa-search search-icon"></i>
									</button>
								</div>
								<input type="text" placeholder="Search ..." class="form-control" name="keyword">
							</div>
						</form>
					</div>
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item toggle-nav-search hidden-caret">
							<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
								<i class="fa fa-search"></i>
							</a>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="/assets/img/profile.jpg" alt="." class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<div class="user-box">
											<div class="avatar-lg"><img src="/assets/img/profile.jpg" alt="image profile" class="avatar-img rounded"></div>
											<div class="u-text">
												<h4><?php echo $_SESSION['nama']; ?></h4>
												<p class="text-muted"><?php echo $_SESSION['email']; ?></p><a href="<?php echo View('profile'); ?>" class="btn btn-xs btn-secondary btn-sm">Profil Saya</a>
											</div>
										</div>
									</li>
									<li>
										<div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="../../action/auth/logout.php">Logout</a>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
		<div class="sidebar sidebar-style-2">
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="/assets/img/profile.jpg" alt="." class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									<?php echo $_SESSION['nama']; ?>
									<span class="user-level"><?php echo $_SESSION['role']; ?></span>
									<span class="caret"></span>
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a href="<?php View('profile'); ?>">
											<span class="link-collapse">Profil Saya</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav nav-primary">
            <?php require(View('menu')); ?>
					</ul>
				</div>
			</div>
		</div>
		<!-- End Sidebar -->

		<div class="main-panel">
			<div class="content">
				<div class="panel-header bg-primary-gradient">
					<div class="page-inner py-5">
						<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
							<div>
								<h2 class="text-white pb-2 fw-bold">Undian</h2>
								<h5 class="text-white op-7 mb-2">Daftar Undian Alfamart</h5>
							</div>
							<div class="ml-md-auto py-2 py-md-0">
                <button type="button" name="button" class="btn btn-primary btn-round btn-border" data-toggle="modal" data-target="#tambahundian">Tambah undian</button>
							</div>
						</div>
					</div>
				</div>
        <div class="page-inner mt--5">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                    Rekap undian
                </div>
                <div class="card-body">
                  <div class="table-responsive">
										<table id="basic-datatables" class="display table table-striped table-hover" >
											<thead>
												<tr>
                          <th>ID</th>
													<th>Nama</th>
													<th>Hadiah</th>
                          <th>Jumlah</th>
													<th>Opsi</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
                          <th>ID</th>
													<th>Nama</th>
													<th>Hadiah</th>
                          <th>Jumlah</th>
													<th>Opsi</th>
												</tr>
											</tfoot>
											<tbody>
                        <?php
                            if ($query){
                              while ($data = mysqli_fetch_row($query)) {
                          ?>
                          <tr>
                            <td><?php echo $data[0] ?></td>
                            <td><?php echo $data[1] ?></td>
                            <td><?php echo $data[2] ?></td>
                            <td><?php echo $data[3] ?></td>
                            <td> <button type="button" data-toggle="modal" data-target="#undian<?php echo $data[0]; ?>" class="btn btn-sm btn-info">Detail</button> | <a href="../../action/admin/hapusUndian.php?id=<?php echo $data[0]; ?>" class="btn btn-danger btn-sm">Hapus</a>  </td>
                          </tr>

                          <div class="modal fade" id="undian<?php echo $data[0]?>" role="dialog">
                            <div class="modal-dialog">
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <center>
                                    <h4>Detail undian</h4>
                                  </center>
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <form  action="../../action/admin/updateUndian.php" method="post">
                                  <div class="modal-body row">
                                    <div class="form-group col-6 col-md-6">
                                      <label>Nama</label>
                                      <input type="text" class="form-control" placeholder="Masukan nama undian" name="nama" value="<?php echo $data[1] ?>" required>
                                    </div>
                                    <div class="form-group col-6 col-md-6">
                                      <label>Minimal Belanja</label>
                                      <input type="text" class="form-control" placeholder="Masukan minimal belanja" name="minimal_belanja" value="<?php echo $data[5] ?>" required>
                                    </div>
                                    <div class="form-group col-6 col-md-3">
                                      <label>Hadiah</label>
                                      <input type="text" class="form-control" placeholder="Masukan hadiah" name="hadiah" value="<?php echo $data[2] ?>" required>
                                    </div>
                                    <div class="form-group col-6 col-md-2">
                                      <label>Jumlah</label>
                                      <input type="number" class="form-control" placeholder="Masukan jumlah hadiah" name="jumlah" value="<?php echo $data[3] ?>" required>
                                    </div>
                                    <div class="form-group col-6 col-md-3">
                                      <label>Harga Hadiah</label>
                                      <input type="number" class="form-control" placeholder="Masukan jumlah hadiah" name="nilai_hadiah" value="<?php echo $data[4] ?>" required>
                                    </div>
                                    <div class="form-group col-6 col-md-4">
                                      <label>Status Pemenang</label>
                                      <input type="text" class="form-control"  name="status" value="<?php if($data[6] == 0){echo "Belum Diumumkan"; } else { echo "Diumumkan";} ?>" required>
                                    </div>

                                    <input type="text" name="id" value="<?php echo $data[0] ?>" hidden>
                                  <div class="modal-footer">
                                    <button type="submit" class="btn btn-info btn-round" name="button">Simpan</button>
                                    <button type="button" class="btn btn-grey" data-dismiss="modal">Kembali</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>




                        <?php
                              }
                            }
                         ?>
											</tbody>
										</table>
									</div>

                </div>

              </div>
            </div>
          </div>
        </div>
			</div>




      <div class="modal fade" id="tambahundian" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <center>
                <h4>Tambah undian</h4>
              </center>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form  action="../../action/admin/tambahUndian.php" method="post">
              <div class="modal-body row">
                <div class="form-group col-6 col-md-6">
                  <label>Nama</label>
                  <input type="text" class="form-control" placeholder="Masukan nama undian" name="nama" required>
                </div>
                <div class="form-group col-6 col-md-6">
                  <label>Minimal Belanja</label>
                  <input type="text" class="form-control" placeholder="Masukan minimal belanja" name="minimal_belanja" required>
                </div>
                <div class="form-group col-6 col-md-4">
                  <label>Hadiah</label>
                  <input type="text" class="form-control" placeholder="Masukan hadiah" name="hadiah" required>
                </div>
                <div class="form-group col-6 col-md-3">
                  <label>Jumlah</label>
                  <input type="number" class="form-control" placeholder="Masukan jumlah hadiah" name="jumlah" required>
                </div>
                <div class="form-group col-6 col-md-3">
                  <label>Harga Hadiah</label>
                  <input type="number" class="form-control" placeholder="Masukan jumlah hadiah" name="nilai_hadiah" required>
                </div>

              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-info btn-round" name="button">Simpan</button>
                <button type="button" class="btn btn-grey" data-dismiss="modal">Kembali</button>
              </div>
            </form>
          </div>
        </div>
      </div>










			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">
						<ul class="nav">
							<li class="nav-item">
								<a class="nav-link" href="">
									ARION
								</a>
							</li>
						</ul>
					</nav>
					<div class="copyright ml-auto">
						<?php echo date('Y'); ?>, made with <i class="fa fa-heart heart text-danger"></i> by <a href="https://siskom.undip.ac.id">Heidi Amelie Ayu Astria</a>
					</div>
				</div>
			</footer>
		</div>

		<!-- Custom template | don't include it in your project! -->
		<!-- End Custom template -->
	</div>
	<!--   Core JS Files   -->
	<script src="/assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="/assets/js/core/popper.min.js"></script>
	<script src="/assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="/assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="/assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>


	<!-- jQuery Sparkline -->
	<script src="/assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Chart Circle -->

	<!-- Datatables -->
	<script src="/assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- Bootstrap Notify -->
	<script src="/assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>
  <script src="/assets/js/plugin/datatables/datatables.min.js"></script>


	<!-- Sweet Alert -->
	<script src="/assets/js/plugin/sweetalert/sweetalert.min.js"></script>

	<!-- Atlantis JS -->
	<script src="/assets/js/atlantis.min.js"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script src="/assets/js/setting-demo.js"></script>
	<script type="text/javascript">
  $(document).ready(function() {
    $('#basic-datatables').DataTable({

    });
  });
  </script>

</body>
</html>
