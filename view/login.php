<?php include('../action/core/url.php') ?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themekita.com/demo-atlantis-bootstrap/livepreview/examples/demo1/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 Nov 2019 06:17:20 GMT -->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Arion | Login</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="https://themekita.com/demo-atlantis-bootstrap/livepreview/examples/assets/img/icon.ico" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="<?php echo BaseUrl(); ?>/assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['<?php echo BaseUrl(); ?>/assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="<?php echo BaseUrl(); ?>/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo BaseUrl(); ?>/assets/css/atlantis.css">
</head>
<body class="login">
	<div class="wrapper wrapper-login">
		<div class="container container-login animated fadeIn">
			<h3 class="text-center">Login Arion</h3>
      <form  action="../action/auth/login.php" method="post">
			<div class="login-form">
				<div class="form-group form-floating-label">
					<input id="username" name="username" type="text" class="form-control input-border-bottom" required>
					<label for="username" class="placeholder">Username</label>
				</div>
				<div class="form-group form-floating-label">
					<input id="password" name="password" type="password" class="form-control input-border-bottom" required>
					<label for="password" class="placeholder">Password</label>
					<div class="show-password">
						<i class="icon-eye"></i>
					</div>
				</div>
				<div class="row form-sub m-0">
				</div>
				<div class="form-action mb-3">
					<button type="submit" name="login" value="login" class="btn btn-primary btn-rounded btn-login">Login</button>
				</div>
				<div class="login-account">
					<span class="msg">Ingin daftar undian</span>
					<a href="#" id="show-signup" class="link">Daftar disini</a>
				</div>
      </form>
			</div>
		</div>

		<div class="container container-signup animated fadeIn">
			<h3 class="text-center">Daftar</h3>
      <form class="" action="../action/tambahSubmit.php" method="post">
        <div class="login-form">

        <div class="form-group form-floating-label">
          <input  name="id_anggota" type="text" class="form-control input-border-bottom" required>
					<label for="id_anggota" class="placeholder">Nomor Anggota</label>
        </div>
				<div class="form-group form-floating-label">
					<select class="" name="id_undian" style="width:350px">
						<?php
							require('../action/core/connection.php');
							$query = $database->query("select * from undian where status=0");
							if ($query) {
								while ($data = mysqli_fetch_row($query)) {
									?>
									<option value="<?php echo $data[0] ?>"><?php echo $data[1]; ?></option>
							<?php
								}
							}
						 ?>
					 </select>
				</div>

				<div class="form-action">
					<a href="#" id="show-signin" class="btn btn-danger btn-link btn-login mr-3">Cancel</a>
          <button type="submit" name="register" class="btn btn-primary btn-rounded btn-login">Daftar</button>
				</div>
			</div>
      </form>

    </div>
	</div>
	<script src="<?php echo BaseUrl(); ?>/assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="<?php echo BaseUrl(); ?>/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="<?php echo BaseUrl(); ?>/assets/js/core/popper.min.js"></script>
	<script src="<?php echo BaseUrl(); ?>/assets/js/core/bootstrap.min.js"></script>
	<script src="<?php echo BaseUrl(); ?>/assets/js/atlantis.min.js"></script>
</body>

<!-- Mirrored from themekita.com/demo-atlantis-bootstrap/livepreview/examples/demo1/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 17 Nov 2019 06:17:25 GMT -->
</html>
