//call base Url
function BaseUrl(){
  //define the url
  $url = "http://".$_SERVER['SERVER_NAME'].'/arion/';
  //send url to caller
  return $url;
}

function View($view){
  //define the url
  $url = "http://".$_SERVER['SERVER_NAME'].'/arion/view/'.$view.'.php';
  //define the url with role
  $urlWithRole = "http://".$_SERVER['SERVER_NAME'].'/arion/view/'.$_SESSION['role'].'/'.$view.'.php';
  //check if url is exist
  if(@get_headers($url)[0]!='HTTP/1.1 404 Not Found'){
    //send url to caller
    return $url;
  //if url with role is  exist
  } elseif(@get_headers($urlWithRole)[0]!='HTTP/1.1 404 Not Found'){
    //send url to caller
    return $urlWithRole;
  //if url is not exist
  } else {
    //warn client that url is not exist
    return "Link that your request are on maintenance or not exist";
  }
}

function Action($action){
  //define the url
  $url1 = '../action/'.$action.'.php';
  $url2 = '../../action/'.$action.'.php';
  //define the url with role
  $urlWithRole1 = '../action/'.($_SESSION['role']).'/'.$action.'.php';
  $urlWithRole2 = '../../action/'.($_SESSION['role']).'/'.$action.'.php';
  //define the url in auth
  $urlInAuth1 = '../action/auth/'.$action.'.php';
  $urlInAuth2 = '../../action/auth/'.$action.'.php';
  //check if url is exist
  if(@get_headers($url1)[0]!='HTTP/1.1 404 Not Found'){
    //send url to caller
    return $url1;
  //if url with role is  exist
  } elseif(@get_headers($url2)[0]!='HTTP/1.1 404 Not Found'){
  //send url to caller
    return $url2;
  //if url with role is  exist
} elseif(@get_headers($urlWithRole1)[0]!='HTTP/1.1 404 Not Found' && isset($_SESSION['role'])){
    //send url to caller
    return $urlWithRole1;
    //if url with role is  exist
  } elseif(@get_headers($urlWithRole2)[0]!='HTTP/1.1 404 Not Found' && isset($_SESSION['role'])){
    //send url to caller
    return $urlWithRole2;
    //if url with role is  exist
  } elseif(@get_headers($urlInAuth1)[0]!='HTTP/1.1 404 Not Found'){
    //send url to caller
    return $urlInAuth1;
  //if url is not exist
  } elseif(@get_headers($urlInAuth2)[0]!='HTTP/1.1 404 Not Found'){
    //send url to caller
    return $urlInAuth2;
  //if url is not exist
  } else {
    //warn client that url is not exist
    return "Link that your request are on maintenance or not exist";
  }
}

function Assets($assets){
  //define the url
  $url = "http://".$_SERVER['SERVER_NAME'].'/arion/assets/'.$assets;
  //check if url is exist
  if(@get_headers($url)[0]!='HTTP/1.1 404 Not Found'){
    //send url to caller
    return $url;
  //if url with role is  exist
  } else {
    //warn client that url is not exist
    return false;
  }
}


function redirect($folder,$url){
  if(strtolower($folder)=='view'){
    header("Location: ".View($url)."");
  } elseif(strtolower($folder)=='action'){
    header("Location: ".Action($url)."");
  }
  exit();
}
